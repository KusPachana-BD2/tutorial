json.array!(@posts) do |post|
  json.extract! post, :id, :forthread_id, :message, :user_id, :location_place
  json.url post_url(post, format: :json)
end
