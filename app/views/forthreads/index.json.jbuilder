json.array!(@forthreads) do |forthread|
  json.extract! forthread, :id, :title, :user_id
  json.url forthread_url(forthread, format: :json)
end
