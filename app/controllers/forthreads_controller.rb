class ForthreadsController < ApplicationController
  before_action :set_forthread, only: [:show, :edit, :update, :destroy]

  # GET /forthreads
  # GET /forthreads.json
  def index
    @forthreads = Forthread.all
  end

  # GET /forthreads/1
  # GET /forthreads/1.json
  def show
  end

  # GET /forthreads/new
  def new
    @forthread = Forthread.new
    @users = User.find :all
  end

  # GET /forthreads/1/edit
  def edit
    @users = User.find :all
  end

  # POST /forthreads
  # POST /forthreads.json
  def create
    @forthread = Forthread.new(forthread_params)

    respond_to do |format|
      if @forthread.save
        format.html { redirect_to @forthread, notice: 'Forum thread was successfully created.' }
        format.json { render action: 'show', status: :created, location: @forthread }
      else
        @users = User.find :all
        format.html { render action: 'new' }
        format.json { render json: @forthread.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /forthreads/1
  # PATCH/PUT /forthreads/1.json
  def update
    respond_to do |format|
      if @forthread.update(forthread_params)
        format.html { redirect_to @forthread, notice: 'Forum thread was successfully updated.' }
        format.json { head :no_content }
      else
        @users = User.find :all
        format.html { render action: 'edit' }
        format.json { render json: @forthread.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /forthreads/1
  # DELETE /forthreads/1.json
  def destroy
    @forthread.transaction do
      @threadPosts = Post.where(forthread_id: @forthread.id)
      @threadPosts.each do |post|
        post.destroy!
      end
      @forthread.destroy!
    end
    respond_to do |format|
      format.html { redirect_to forthreads_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_forthread
      @forthread = Forthread.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def forthread_params
      params.require(:forthread).permit(:title, :user_id)
    end
end
