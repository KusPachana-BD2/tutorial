class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :login
      t.string :password
      t.string :city
      t.integer :age
      t.string :gender

      t.timestamps
    end
  end
end
