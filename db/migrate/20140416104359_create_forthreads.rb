class CreateForthreads < ActiveRecord::Migration
  def change
    create_table :forthreads do |t|
      t.string :title
      t.integer :user_id

      t.timestamps
    end
  end
end
