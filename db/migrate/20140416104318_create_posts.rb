class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.integer :forthread_id
      t.text :message
      t.integer :user_id
      t.string :location_place

      t.timestamps
    end
  end
end
