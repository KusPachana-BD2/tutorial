require 'test_helper'

class ForthreadsControllerTest < ActionController::TestCase
  setup do
    @forthread = forthreads(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:forthreads)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create forthread" do
    assert_difference('Forthread.count') do
      post :create, forthread: { title: @forthread.title, user_id: @forthread.user_id }
    end

    assert_redirected_to forthread_path(assigns(:forthread))
  end

  test "should show forthread" do
    get :show, id: @forthread
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @forthread
    assert_response :success
  end

  test "should update forthread" do
    patch :update, id: @forthread, forthread: { title: @forthread.title, user_id: @forthread.user_id }
    assert_redirected_to forthread_path(assigns(:forthread))
  end

  test "should destroy forthread" do
    assert_difference('Forthread.count', -1) do
      delete :destroy, id: @forthread
    end

    assert_redirected_to forthreads_path
  end
end
